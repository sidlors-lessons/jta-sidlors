package mx.sidlors.mapper;

import mx.sidlors.dto.PrestamoDTO;
import mx.sidlors.model.Prestamo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PrestamoMapper {
    PrestamoMapper INSTANCE = Mappers.getMapper(PrestamoMapper.class);

    /**
     * Converts a Prestamo object to a PrestamoDTO object.
     *
     * @param  prestamo  the Prestamo object to convert
     * @return           the converted PrestamoDTO object
     */
    PrestamoDTO toDto(Prestamo prestamo);

    /**
     * Converts a PrestamoDTO object to a Prestamo entity.
     *
     * @param  dto  the PrestamoDTO object to be converted
     * @return      the Prestamo entity
     */
    Prestamo toEntity(PrestamoDTO dto);

    /**
     * Converts a list of Prestamo objects to a list of PrestamoDTO objects.
     *
     * @param  prestamos  the list of Prestamo objects to be converted
     * @return            the list of PrestamoDTO objects
     */
    List<PrestamoDTO> toDtoList(List<Prestamo> prestamos);
}
