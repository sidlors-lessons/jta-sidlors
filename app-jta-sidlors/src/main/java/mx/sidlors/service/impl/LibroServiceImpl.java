package mx.sidlors.service.impl;

import jakarta.transaction.Transactional;
import mx.sidlors.dao.LibroDAO;
import mx.sidlors.model.Libro;
import mx.sidlors.service.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LibroServiceImpl implements LibroService {
    private final LibroDAO libroDAO;
    @Autowired
    public LibroServiceImpl(LibroDAO libroDAO){
        this.libroDAO=libroDAO;
    }
    /**
     * Check if the book is available in the library.
     *
     * @param  libro   the book to check availability for
     * @return         true if the book is available, false otherwise
     */
    @Override
    @Transactional
    public boolean estaDispobleLibro(Libro libro) {
        return libroDAO.obtenerLibroPorId(libro.getId()).getCantidadDisponible() > 0 ;
    }

    /**
     * Actualiza un libro en la base de datos.
     *
     * @param  libro  el libro que se va a actualizar
     * @return        no devuelve ningún valor
     */
    @Override

    @Transactional
    public void actualizarLibro(Libro libro) {
        libroDAO.actualizarLibro(libro);
    }

    @Override
    @Transactional
    public Libro obtenerLibroPorISBN(Long isbn) {
        return libroDAO.obtenerLibroPorId(isbn);
    }
}
