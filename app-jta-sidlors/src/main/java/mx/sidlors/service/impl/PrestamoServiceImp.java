package mx.sidlors.service.impl;

import jakarta.transaction.Transactional;
import mx.sidlors.dao.PrestamoDAO;
import mx.sidlors.dto.PrestamoDTO;
import mx.sidlors.exepcion.PrestamoException;
import mx.sidlors.mapper.PrestamoMapper;
import mx.sidlors.model.Estudiante;
import mx.sidlors.model.Libro;
import mx.sidlors.model.Prestamo;
import mx.sidlors.service.EstudianteService;
import mx.sidlors.service.LibroService;
import mx.sidlors.service.PrestamoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class PrestamoServiceImp implements PrestamoService{

    @Value("${app.prestamo.dias}")
    private String diasPrestamo;

    @Value("${app.total.prestamo}")
    private String maxPrestamos;

    private final PrestamoDAO prestamoDAO;
    private final EstudianteService estudianteService;
    private final LibroService libroService;

    @Autowired
    public PrestamoServiceImp(PrestamoDAO prestamoDAO, EstudianteService estudianteService,
                            LibroService libroService) {
        this.prestamoDAO = prestamoDAO;
        this.estudianteService = estudianteService;
        this.libroService = libroService;
    }



    /**
     * altaPrestamo function to create a new loan for a student with a book
     *
     * @param  estudiante	description of parameter
     * @param  libro	description of parameter
     * @return         	PrestamoDTO object representing the created loan
     */
    @Override
    @Transactional
    public PrestamoDTO altaPrestamo(Estudiante estudiante, Libro libro) throws PrestamoException {
        if(validaPrestamo(estudiante) && validaDisponibilidad(libro)){
            libro.setCantidadDisponible(libro.getCantidadDisponible() - 1);
            libroService.actualizarLibro(libro);
            Prestamo nuevoPrestamo = Prestamo.builder()
                    .estudiante(estudiante)
                    .libro(libro)
                    .build();
            // Configurar la fecha de prestamo como la fecha actual del sistema
            LocalDateTime localDateTime = LocalDateTime.now();
            nuevoPrestamo.setFechaPrestamo(localDateTime);
            nuevoPrestamo.setFechaDevolucion(localDateTime.plusDays(Integer.getInteger(diasPrestamo)));

            Prestamo prestamo = prestamoDAO.actualizarPrestamo(nuevoPrestamo);
            return PrestamoMapper.INSTANCE.toDto(prestamo);
        }
        throw new PrestamoException("Libro no disponible para usuario");
    }

    /**
     * devolverPrestamo function returns the PrestamoDTO for the given Estudiante and Libro
     *
     * @param  estudiante  the Estudiante object
     * @param  libro       the Libro object
     * @return             the PrestamoDTO object
     */
    @Override
    @Transactional
    public PrestamoDTO devolverPrestamo(Estudiante estudiante, Libro libro) throws PrestamoException {

        Prestamo prestamo = prestamoDAO.obtenerPrestamoPorEstudianteYLibro(estudiante.getId(),libro.getId());
        if(prestamo != null){
            prestamo.setFechaDevolucion(LocalDateTime.now());
            Libro updatedLibro = prestamo.getLibro();
            updatedLibro.setCantidadDisponible(updatedLibro.getCantidadDisponible() + 1);
            prestamo.setLibro(updatedLibro);
            prestamo.setDevuelto(true);
            prestamoDAO.actualizarPrestamo(prestamo);
            return PrestamoMapper.INSTANCE.toDto(prestamo);
        }
        throw new PrestamoException("Prestamo no encontrado");
    }

    /**
     * Validates the availability of a book.
     *
     * @param  libro   the book to be validated
     * @return         true if the book is available, false otherwise
     */
    @Transactional
    public boolean validaDisponibilidad(Libro libro) {
        return libroService.estaDispobleLibro(libro);
    }

    @Override
    @Transactional
    public List<PrestamoDTO> obtenerTodosLosPrestamos() {
        List<Prestamo> prestamos = prestamoDAO.obtenerPrestamos();
        return PrestamoMapper.INSTANCE.toDtoList(prestamos);
    }

    @Override
    @Transactional
    public List<PrestamoDTO> obtenerPrestamos(Libro libro) {
        List<Prestamo> prestamos = prestamoDAO.obtenerPrestamosPorLibro(libro.getId());
        return PrestamoMapper.INSTANCE.toDtoList(prestamos);
    }

    @Override
    @Transactional
    public List<PrestamoDTO> devuelveTodosLosPrestamosPendientes() {
        List<Prestamo> prestamos = prestamoDAO.obtenerPrestamosPendientes();
        return PrestamoMapper.INSTANCE.toDtoList(prestamos);
    }

    /**
     * Validates a loan for a student.
     *
     * @param  estudiante   the student for whom the loan is being validated
     * @return              true if the student's loan is valid, false otherwise
     */
    @Transactional
    private boolean validaPrestamo(Estudiante estudiante) {
            return estudianteService.obtenerCantidadLibrosPrestadosPorEstudiante(estudiante.getId())
                > Integer.getInteger(maxPrestamos);
    }




}
