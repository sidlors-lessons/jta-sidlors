package mx.sidlors.service.impl;

import jakarta.transaction.Transactional;
import mx.sidlors.dao.EstudianteDAO;
import mx.sidlors.model.Estudiante;
import mx.sidlors.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstudianteServiceImp implements EstudianteService {

    private final EstudianteDAO estudianteDAO;

    @Autowired
    public EstudianteServiceImp(EstudianteDAO estudianteDAO){
        this.estudianteDAO=estudianteDAO;
    }


    /**
     * Obtains the quantity of books borrowed by a student.
     *
     * @param  idEstudiante   the ID of the student
     * @return                the quantity of borrowed books
     */
    @Override
    @Transactional
    public long obtenerCantidadLibrosPrestadosPorEstudiante(Long idEstudiante) {
       return estudianteDAO.obtenerCantidadLibrosPrestadosPorEstudiante(idEstudiante);
    }

    @Override
    @Transactional
    public List<Estudiante> obtenerEstudiantes() {
        return estudianteDAO.obtenerTodosLosEstudiantes();
    }

    @Override
    @Transactional
    public Estudiante obtenerEstudiantePorCarne(Estudiante estudiante) {
        return estudianteDAO.obtenerEstudiantePorCarne(estudiante.getCarnet());
    }
}
