package mx.sidlors.service;

import mx.sidlors.model.Libro;

public interface LibroService {

    /**
     * A description of the entire Java function.
     *
     * @param  libro	description of parameter
     * @return         description of return value
     */
    public boolean estaDispobleLibro(Libro libro);

    /**
     * Updates the information of the given book.
     *
     * @param  libro  the book object to be updated
     */
    public void actualizarLibro(Libro libro);

    /**
     * A description of the entire Java function.
     *
     * @param  isbn	description of parameter
     * @return         description of return value
     */
    public Libro obtenerLibroPorISBN(Long isbn);

}
