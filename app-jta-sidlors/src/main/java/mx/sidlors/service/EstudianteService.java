package mx.sidlors.service;

import mx.sidlors.model.Estudiante;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EstudianteService {

    /**
     * Obtains the quantity of books borrowed by a student.
     *
     * @param  idEstudiante   the ID of the student
     * @return                the quantity of borrowed books
     */
    public long obtenerCantidadLibrosPrestadosPorEstudiante(Long idEstudiante) ;

    /**
     * A description of the entire Java function.
     *
     * @return         	description of return value
     */
    public List<Estudiante> obtenerEstudiantes();

    /**
     * A description of the entire Java function.
     *
     * @return         	description of return value
     */
    public Estudiante obtenerEstudiantePorCarne(Estudiante estudiante);


}
