package mx.sidlors.service;

import mx.sidlors.dto.PrestamoDTO;
import mx.sidlors.exepcion.PrestamoException;
import mx.sidlors.model.Estudiante;
import mx.sidlors.model.Libro;

import java.util.List;

public interface PrestamoService {

    /**
     * altaPrestamo method is used to create a new loan for a student.
     *
     * @param  estudiante  the student who is borrowing the book
     * @param  libro       the book being borrowed
     * @return             the created loan information
     */
    public PrestamoDTO altaPrestamo(Estudiante estudiante, Libro libro) throws PrestamoException;

    /**
     * devolverPrestamo method to return a PrestamoDTO for the given Estudiante and Libro
     *
     * @param  estudiante  the Estudiante object
     * @param  libro       the Libro object
     * @return             the PrestamoDTO object
     */
    public PrestamoDTO devolverPrestamo(Estudiante estudiante, Libro libro) throws PrestamoException;

    /**
     * Validates the availability of a book.
     *
     * @param  libro  the book to validate
     * @return        true if the book is available, false otherwise
     */
    public boolean validaDisponibilidad(Libro libro);

    /**
     * A description of the entire Java function.
     *
     * @return         	description of return value
     */
    public List<PrestamoDTO> obtenerTodosLosPrestamos();


    /**
     * Obtains a list of PrestamoDTO objects related to the given Libro object.
     *
     * @param  libro  the Libro object for which to obtain the list of PrestamoDTO objects
     * @return       a list of PrestamoDTO objects related to the given Libro object
     */
    public List<PrestamoDTO> obtenerPrestamos(Libro libro);

    public List<PrestamoDTO> devuelveTodosLosPrestamosPendientes();
}
