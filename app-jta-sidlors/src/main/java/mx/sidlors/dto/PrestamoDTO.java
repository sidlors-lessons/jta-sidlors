package mx.sidlors.dto;

import lombok.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PrestamoDTO {

    @Getter
    @Setter
    private EstudianteDTO estudiante;

    @Getter
    @Setter
    private LibroDTO libro;

    private LocalDateTime fechaPrestamo;

    private LocalDateTime fechaDevolucion;

    private Boolean devuelto;


}
