package mx.sidlors.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LibroDTO {

    private String titulo;
    private String autor;
    private String isbn;
    private Integer cantidadDisponible;

}
