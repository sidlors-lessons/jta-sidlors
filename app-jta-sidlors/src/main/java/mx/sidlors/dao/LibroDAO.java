package mx.sidlors.dao;

import mx.sidlors.model.Libro;
import org.springframework.stereotype.Repository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;

@Repository
public class LibroDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void guardarLibro(Libro libro) {
        entityManager.persist(libro);
    }

    public Libro obtenerLibroPorId(Long libroId) {
        return entityManager.find(Libro.class, libroId);
    }

    public List<Libro> obtenerTodosLosLibros() {
        return entityManager.createQuery("SELECT l FROM Libro l", Libro.class).getResultList();
    }

    public void actualizarLibro(Libro libro) {
        entityManager.merge(libro);
    }

    public void eliminarLibro(Long libroId) {
        Libro libro = obtenerLibroPorId(libroId);
        if (libro != null) {
            entityManager.remove(libro);
        }
    }
}
