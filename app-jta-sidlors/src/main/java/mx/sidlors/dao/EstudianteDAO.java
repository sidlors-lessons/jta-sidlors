package mx.sidlors.dao;

import mx.sidlors.model.Estudiante;
import mx.sidlors.model.Prestamo;
import org.springframework.stereotype.Repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.*;
import java.util.List;

@Repository
public class EstudianteDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void guardarEstudiante(Estudiante estudiante) {
        entityManager.persist(estudiante);
    }

    public Estudiante obtenerEstudiantePorId(Long estudianteId) {
        return entityManager.find(Estudiante.class, estudianteId);
    }

    public List<Estudiante> obtenerTodosLosEstudiantes() {
        return entityManager.createQuery("SELECT e FROM Estudiante e", Estudiante.class).getResultList();
    }

    public void actualizarEstudiante(Estudiante estudiante) {
        entityManager.merge(estudiante);
    }

    public void eliminarEstudiante(Long estudianteId) {
        Estudiante estudiante = obtenerEstudiantePorId(estudianteId);
        if (estudiante != null) {
            entityManager.remove(estudiante);
        }
    }

    public long obtenerCantidadLibrosPrestadosPorEstudiante(Long idEstudiante) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<Prestamo> prestamoRoot = criteriaQuery.from(Prestamo.class);
        Join<Prestamo, Estudiante> estudianteJoin = prestamoRoot.join("estudiante", JoinType.INNER);

        Predicate predicadoEstudiante = criteriaBuilder.equal(estudianteJoin.get("id"), idEstudiante);

        criteriaQuery.select(criteriaBuilder.count(prestamoRoot))
                .where(predicadoEstudiante);

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public Estudiante obtenerEstudiantePorCarne(String carnet) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Estudiante> criteriaQuery = criteriaBuilder.createQuery(Estudiante.class);
        Root<Estudiante> estudianteRoot = criteriaQuery.from(Estudiante.class);
        Predicate predicadoCarnet = criteriaBuilder.equal(estudianteRoot.get("carnet"), carnet);
        criteriaQuery.select(estudianteRoot).where(predicadoCarnet);
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }
}

