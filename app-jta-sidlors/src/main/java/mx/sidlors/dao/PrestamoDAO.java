package mx.sidlors.dao;

import jakarta.persistence.PersistenceContextType;
import mx.sidlors.model.Prestamo;
import org.springframework.stereotype.Repository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import java.util.List;

@Repository
public class PrestamoDAO {

    @PersistenceContext(type=PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public void guardarPrestamo(Prestamo prestamo) {
        entityManager.persist(prestamo);
    }

    public Prestamo obtenerPrestamoPorId(Long prestamoId) {
        return entityManager.find(Prestamo.class, prestamoId);
    }

    public List<Prestamo> obtenerTodosLosPrestamos() {
        return entityManager.createQuery("SELECT p FROM Prestamo p", Prestamo.class).getResultList();
    }

    public Prestamo actualizarPrestamo(Prestamo prestamo) {
        return entityManager.merge(prestamo);
    }

    public void eliminarPrestamo(Long prestamoId) {
        Prestamo prestamo = obtenerPrestamoPorId(prestamoId);
        if (prestamo != null) {
            entityManager.remove(prestamo);
        }
    }

    public boolean validaPosiblePrestamo(Long estudianteId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Prestamo> prestamoRoot = criteriaQuery.from(Prestamo.class);

        criteriaQuery.select(criteriaBuilder.count(prestamoRoot));
        criteriaQuery.where(criteriaBuilder.equal(prestamoRoot.get("estudiante").get("estudianteId"), estudianteId));

        Long cantidadPrestamos = entityManager.createQuery(criteriaQuery).getSingleResult();

        return cantidadPrestamos > 3;
    }

    public Prestamo obtenerPrestamoPorEstudianteYLibro(Long id, Long id1) {
        return entityManager.createQuery("SELECT p FROM Prestamo p WHERE p.estudiante.id = :id AND p.libro.id = :id1", Prestamo.class)
                .setParameter("id", id)
                .setParameter("id1", id1)
                .getSingleResult();
    }

    public List<Prestamo> obtenerPrestamosPorLibro(Long id) {
        return entityManager.createQuery("SELECT p FROM Prestamo p WHERE p.libro.id = :id", Prestamo.class)
                .setParameter("id", id)
                .getResultList();
    }

    /**
     * Get the list of Prestamo objects representing the obtained loans.
     *
     * @return         	the list of Prestamo objects representing the obtained loans
     */
    public List<Prestamo> obtenerPrestamos() {
        return entityManager.createQuery("SELECT p FROM Prestamo p", Prestamo.class).getResultList();
    }
    public List<Prestamo> obtenerPrestamosPendientes() {
        return entityManager.createQuery("SELECT p FROM Prestamo p WHERE p.devuelto = false", Prestamo.class).getResultList();
    }
}

