package mx.sidlors.model;
import lombok.*;

import jakarta.persistence.*;

@Entity
@Table(name = "libros")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Libro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "libro_id")
    private Long id;

    private String titulo;

    private String autor;

    private String isbn;

    @Column(name = "cantidad_disponible")
    private Integer cantidadDisponible;
}
