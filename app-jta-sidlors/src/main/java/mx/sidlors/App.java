package mx.sidlors;

import lombok.extern.slf4j.Slf4j;
import mx.sidlors.config.AppConfig;
import mx.sidlors.dto.PrestamoDTO;
import mx.sidlors.exepcion.PrestamoException;
import mx.sidlors.model.Estudiante;
import mx.sidlors.model.Libro;
import mx.sidlors.service.EstudianteService;
import mx.sidlors.service.PrestamoService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Slf4j
public class App {

    public static void main(String[] args) {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class)) {
            EstudianteService estudianteService = context.getBean(EstudianteService.class);
            PrestamoService prestamoService = context.getBean(PrestamoService.class);
            long l = estudianteService.obtenerCantidadLibrosPrestadosPorEstudiante(1L);
            log.info("Total de libros prestados: {}", l);
            log.info("Listado de prestamos que no se han devuelto: {}",prestamoService.devuelveTodosLosPrestamosPendientes()
                    .stream()
                    .map(PrestamoDTO::toString)
                    .reduce("", (a, b) -> a + "\n" + b));

            PrestamoDTO prestamo = prestamoService
                        .devolverPrestamo(Estudiante.builder()
                                .id(1L).build(),
                                Libro.builder().id(1L).build());
            log.info("Prestamo devuelto: {}", prestamo);

        } catch (PrestamoException e) {
            log.error("Error en la operación de préstamo: {}", e.getMessage(), e);
        }
        finally {
            log.info("Fin de la aplicacion");
        }
    }
}

