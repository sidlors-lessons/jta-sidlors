CREATE TABLE estudiantes (
    estudiante_id SERIAL PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    apellido VARCHAR(100) NOT NULL,
    carnet VARCHAR(20) UNIQUE NOT NULL
);

CREATE TABLE libros (
    libro_id SERIAL PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    autor VARCHAR(100) NOT NULL,
    isbn VARCHAR(20) UNIQUE NOT NULL,
    cantidad_disponible INTEGER NOT NULL
);

CREATE TABLE prestamos (
    prestamo_id SERIAL PRIMARY KEY,
    estudiante_id INTEGER REFERENCES estudiantes(estudiante_id),
    libro_id INTEGER REFERENCES libros(libro_id),
    fecha_prestamo TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    fecha_devolucion TIMESTAMP,
    devuelto BOOLEAN DEFAULT FALSE,
    CONSTRAINT fk_prestamo_estudiante FOREIGN KEY (estudiante_id) REFERENCES estudiantes(estudiante_id),
    CONSTRAINT fk_prestamo_libro FOREIGN KEY (libro_id) REFERENCES libros(libro_id)
);

INSERT INTO estudiantes (nombre, apellido, carnet) VALUES
('Pepito', 'Perez', '203215834'),
('Albert', 'Einstein', 'A12345'),
('Marie', 'Curie', 'B23456'),
('William', 'Shakespeare', 'C34567'),
('Jane', 'Goodall', 'D45678'),
('Nelson', 'Mandela', 'E56789'),
('Rosa', 'Parks', 'F67890'),
('Leonardo', 'da Vinci', 'G78901'),
('Emily', 'Dickinson', 'H89012'),
('Malala', 'Yousafzai', 'I90123'),
('Isaac', 'Newton', 'J01234');


INSERT INTO libros (titulo, autor, isbn, cantidad_disponible) VALUES
('Pedro Paramo', 'Juan Rulfo', 'ISBN del Libro', 10),
('1984', 'George Orwell', '9780451524935', 10),
('To Kill a Mockingbird', 'Harper Lee', '9780061120084', 15),
('The Great Gatsby', 'F. Scott Fitzgerald', '9780743273565', 8),
('The Catcher in the Rye', 'J.D. Salinger', '9780241950425', 12),
('Harry Potter and the Philosopher''s Stone', 'J.K. Rowling', '9781408855652', 20),
('The Hobbit', 'J.R.R. Tolkien', '9780547928227', 18),
('The Lord of the Rings', 'J.R.R. Tolkien', '9780544003415', 25),
('Pride and Prejudice', 'Jane Austen', '9780143105428', 14),
('The Da Vinci Code', 'Dan Brown', '9780307474278', 17),
('The Alchemist', 'Paulo Coelho', '9780061122415', 22);


-- Inserts aleatorios para la tabla de prestamos
INSERT INTO prestamos (estudiante_id, libro_id, fecha_prestamo, fecha_devolucion, devuelto) VALUES
(1, 1, '2024-02-01', '2024-02-05',FALSE),
(2, 1, '2024-02-02', '2024-02-06',TRUE),
(3, 3, '2024-02-03', '2024-02-07',TRUE),
(4, 4, '2024-02-04', '2024-02-08',TRUE),
(4, 1, '2024-02-05', '2024-02-09',FALSE),
(6, 6, '2024-02-06', '2024-02-10',TRUE),
(8, 7, '2024-02-07', '2024-02-11',FALSE),
(8, 8, '2024-02-08', '2024-02-12',TRUE),
(8, 2, '2024-02-09', '2024-02-13',FALSE),
(10, 4, '2024-02-10', '2024-02-14',TRUE);